$(document).ready(function(){
	var $jours = $('.jour');
	var $puces = $('.bullets .entypo-record		'); // Mauvaise indentation

	function init(){ // Mauvaise indentation
		setTimeout(function(){
			$('body').addClass('isok');
			$jours.hide();
			$('.wrapper').fadeIn('slow', function(){
				$jours.first().fadeIn('slow');
				$puces.removeClass('active').first().addClass('active'); // Manque les parenthèses après "first"
			});
		}, 2000);
		
	}; // Mauvaise indentation + manque ";"

	$puces.click(function(){ // Manque un "s" à "puces"
		var $this = $(this); // Mauvaise indentation
		var cible = $this.attr('data-cible');
		$jours.hide();

		$($jours.get(cible)).fadeIn(); // Manque ";" à la fin
		$puces.removeClass('active'); 
		$this.addClass('active');
	});
	init();
});  // Manque "}"
